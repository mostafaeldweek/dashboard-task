import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";
import Users from "@/views/Users.vue";
import Statistics from "@/views/Statistics.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/Users",
    name: "users",
    component: Users,
  },
  {
    path: "/statistics",
    name: "statistics",
    component: Statistics,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
