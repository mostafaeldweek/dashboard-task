import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import i18n from "./i18n";
import Vuelidate from "vuelidate";
import "./assets/scss/main.scss";
import "./plugins/validators";

import axiosInstance from "./axios/index";

Vue.config.productionTip = false;
Vue.prototype.$axios = axiosInstance;

new Vue({
  router,
  store,
  vuetify,
  i18n,
  Vuelidate,
  render: function (h) {
    return h(App);
  },
}).$mount("#app");
