import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com",
  timeout: 5000,
});


export default axiosInstance;

// axiosInstance.interceptors.request.use(
//   function (config) {
//     console.log("Request Interceptor:", config);
//     return config;
//   },
//   function (error) {
//     return Promise.reject(error);
//   }
// );
// axiosInstance.interceptors.response.use(
//   function (response) {
//     console.log("Response Interceptor:", response);
//     response.data.statusCode = response.status;
//     const { status } = response;
//     if (status >= 200 && status < 300) {
//       console.log("Request successful");
//     } else {
//       console.error("Error response:", status);
//     }
//     return response;
//   },
//   function (error) {
//     if (error.response) {
//       error.response.data.statusCode = error.response.status;
//       console.error("Error response:", error.response.status);
//     } else if (error.request) {
//       console.error("No response received:", error.message);
//     } else {
//       console.error("Request error:", error.message);
//     }
//     return Promise.reject(error);
//   }
// );
